
const { parseCommandLineArguments } = require('./utils/commandLineUtils');
const { filterDataByKeyword } = require('./utils/filterUtils');
const { countItems } = require('./utils/countUtils');

const { data } = require('./data/data.js');

const main = () => {
  const option = parseCommandLineArguments();

  switch (option.key) {
    case 'filter':
      const filteredData = filterDataByKeyword(data, option.value);
      console.log(filteredData);
      break;
    case 'count':
      const countData = countItems(data);
      console.log(countData);
      break;
    default:
      break;
  }
}

main()
