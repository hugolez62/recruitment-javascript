const filterAnimalsByKeyword = (animals, keyword) => {
  return animals.filter(animal => {
    return animal.name.toLowerCase().includes(keyword.toLowerCase());
  });
};

const filterPeopleByKeyword = (people, keyword) => {
  return people.reduce((filteredPeople, person) => {
    const filteredAnimals = filterAnimalsByKeyword(person.animals, keyword);
    if (filteredAnimals.length > 0) {
      filteredPeople.push({
        name: person.name,
        animals: filteredAnimals
      });
    }
    return filteredPeople;
  }, []);
};

const filterDataByKeyword = (data, keyword) => {
  const filteredData = data.reduce((filteredData, item) => {
    const filteredPeople = filterPeopleByKeyword(item.people, keyword);
    if (filteredPeople.length > 0) {
      filteredData.push({
        name: item.name,
        people: filteredPeople
      });
    }
    return filteredData;
  }, []);

  return filteredData.length > 0 ? filteredData : 'No animals match the search';
};

module.exports = {
  filterDataByKeyword,
  filterAnimalsByKeyword,
  filterPeopleByKeyword
};
