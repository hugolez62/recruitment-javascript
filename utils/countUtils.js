const countItems = (data) => {
  return data.map(item => {
    const peopleCount = item.people.length;
    const countString = `[${peopleCount}]`;
    return {
      name: `${item.name} ${countString}`,
      people: item.people.map(person => {
        const animalCount = person.animals.length;
        return {
          name: `${person.name} [${animalCount}]`,
          animals: person.animals
        };
      })
    };
  });
};

module.exports = {
  countItems
};
