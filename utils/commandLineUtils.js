const parseCommandLineArguments = () => {
  const args = process.argv.slice(2);
  const options = {};
  const [key, value] = args[0].split('=');

  try {
    if (args.length > 1) {
      throw new Error("Please provide only one argument.");
    }

    if (!key.startsWith('--')) {
      throw new Error(`Invalid argument format: ${key}. Please use --key=value format.`);
    }

    if (key === '--filter' && value === undefined) {
      throw new Error(`--filter option requires a value.`);
    }

    options['key'] = key.substring(2);
    options['value'] = value;

  } catch (error) {
    throw new Error(error.message);
  }

  return options;
}

module.exports = {
  parseCommandLineArguments
};
