# Node.js Data Filtering and Counting

This project is a simple Node.js application that allows filtering and counting data from a given dataset. It offers two main functionalities: filtering data by keyword and counting elements in the data.

## Installation

Before running the application, ensure that you have Node.js installed on your machine. You can download and install it from [the official Node.js website](https://nodejs.org/).

Once Node.js is installed, follow these steps to install the project dependencies:

1. Clone this repository to your local machine:

    ```
    git clone <repository_url>
    ```

2. Navigate to the project directory:

    ```
    cd project_name
    ```

3. Install dependencies by running the following command:

    ```
    npm install
    ```

## Usage

The application can be executed from the command line using `node app.js` followed by the necessary options. Here are the available options:

- `--filter=keyword`: Filters data based on the specified keyword.
- `--count`: Counts elements in the data.

Here are some usage examples:

1. Filtering data based on a keyword:

    ```
    node app.js --filter=keyword
    ```

2. Counting elements in the data:

    ```
    node app.js --count
    ```

## Tests

The project includes unit tests for various functions as well as integration tests for the main application. To run the tests, use the following command:

```
npx jest
```
