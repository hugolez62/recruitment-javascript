const { parseCommandLineArguments } = require('../utils/commandLineUtils');

describe('parseCommandLineArguments', () => {
  it('should parse command line arguments correctly', () => {
    process.argv = ['node', 'app.js', '--filter=ry'];

    const options = parseCommandLineArguments();

    expect(options.key).toBe('filter');
    expect(options.value).toBe('ry');
  });

  it('should throw error when provided with multiple arguments', () => {
    process.argv = ['node', 'app.js', '--filter=value', '--count'];

    expect(() => {
      parseCommandLineArguments();
    }).toThrowError('Please provide only one argument.');
  });

  it('should throw error for invalid argument format', () => {
    process.argv = ['node', 'app.js', 'filter=value'];

    expect(() => {
      parseCommandLineArguments();
    }).toThrowError('Invalid argument format: filter. Please use --key=value format.');
  });

  it('should throw error when --filter option is missing a value', () => {
    process.argv = ['node', 'app.js', '--filter'];

    expect(() => {
      parseCommandLineArguments();
    }).toThrowError('--filter option requires a value.');
  });
});
