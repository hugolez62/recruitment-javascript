const {
  filterAnimalsByKeyword,
  filterPeopleByKeyword,
  filterDataByKeyword
} = require('../utils/filterUtils');

describe('filterAnimalsByKeyword', () => {
  it('should filter animals by keyword', () => {
    const animals = [
      { name: 'Duck' },
      { name: 'Narwhal' },
      { name: 'Badger' }
    ];
    const keyword = 'ge';

    const filteredAnimals = filterAnimalsByKeyword(animals, keyword);

    expect(filteredAnimals).toEqual([
      { name: 'Badger' }
    ]);
  });
});

describe('filterPeopleByKeyword', () => {
  it('should filter people by keyword', () => {
    const people = [
      { name: 'Winifred Graham', animals: [{ name: 'Duck' }, { name: 'Narwhal' }] },
      { name: 'Blanche Viciani', animals: [{ name: 'Rabbit' }] }
    ];
    const keyword = 'bb';

    const filteredPeople = filterPeopleByKeyword(people, keyword);

    expect(filteredPeople).toEqual([
      { name: 'Blanche Viciani', animals: [{ name: 'Rabbit' }] }
    ]);
  });
});

describe('filterDataByKeyword', () => {
  it('should filter data by keyword', () => {
    const data = [
      { name: 'Dillauti', people: [{ name: 'Winifred Graham', animals: [{ name: 'Duck' }, { name: 'Narwhal' }] }] },
      { name: 'Tohabdal', people: [{ name: 'Effie Houghton', animals: [{ name: 'Zebra' }] }] }
    ];
    const keyword = 'Ze';

    const filteredData = filterDataByKeyword(data, keyword);

    expect(filteredData).toEqual([
      { name: 'Tohabdal', people: [{ name: 'Effie Houghton', animals: [{ name: 'Zebra' }] }] }
    ]);
  });

  it('should return message for no matching animals', () => {
    const data = [
      { name: 'Dillauti', people: [{ name: 'Winifred Graham', animals: [{ name: 'Duck' }, { name: 'Narwhal' }] }] },
      { name: 'Tohabdal', people: [{ name: 'Effie Houghton', animals: [{ name: 'Zebra' }] }] }
    ];
    const keyword = 'Ef';

    const filteredData = filterDataByKeyword(data, keyword);

    expect(filteredData).toBe('No animals match the search');
  });
});
