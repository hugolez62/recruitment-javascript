const { countItems } = require('../utils/countUtils');

describe('countItems', () => {
  it('should correctly count items', () => {
    const data = [
      { name: 'Dillauti', people: [{ name: 'Winifred Graham', animals: [{ name: 'Duck' }, { name: 'Narwhal' }] }] },
      { name: 'Tohabdal', people: [{ name: 'Effie Houghton', animals: [{ name: 'Zebra' }] }] }
    ];

    const countedData = countItems(data);

    expect(countedData).toEqual([
      {
        name: 'Dillauti [1]',
        people: [{ name: 'Winifred Graham [2]', animals: [{ name: 'Duck' }, { name: 'Narwhal' }] }]
      },
      {
        name: 'Tohabdal [1]',
        people: [{ name: 'Effie Houghton [1]', animals: [{ name: 'Zebra' }] }]
      }
    ]);
  });
});
